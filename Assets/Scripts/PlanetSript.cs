using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlanetSript : MonoBehaviour, IPointerClickHandler
{
    #region Planet stats
    private float planet_size; //Planet radius
    [HideInInspector] public int planet_ship; //How more ships on the planet
    #endregion

    #region Other
    //Material form marking the planet
    [SerializeField] private Material marker_Enemy;
    [SerializeField] private Material marker_Player;
    [SerializeField] private Material marker_Glow_Player;

    GenerateSettings GenSettings;

    protected Text info; //information. How more ship on the planet 
    [HideInInspector] public string belongs = "Neutral";

    protected float Timer;
    public int planet_ship_construct = 5; //Construck ship on the planet every tick
    private int DelayAmount = 1; // Second count (1 = 1 sec)
    #endregion

    private void Start()
    {
        gameObject.SetActive(true);
    }

    void Awake()
    {
        if (belongs == "Neutral")
            planet_ship = Random.Range(15, 100); // Neutral
        else
            planet_ship = 50; //Enemy, Player

        GenSettings = GameObject.FindGameObjectWithTag("Gen").GetComponent<GenerateSettings>();
        info = transform.GetChild(0).GetChild(0).GetComponentInChildren<Text>();

        Resize();
        Reposition();
    }

    void Resize()
    {
        planet_size = Random.Range(GenSettings.minPlanetSize, GenSettings.maxPlanetSize);

        transform.localScale = new Vector3(planet_size, planet_size, 1);
    }

    void Reposition()
    {
        var x = Random.Range(GenSettings.cam_ZeroPosition.x + planet_size, -GenSettings.cam_ZeroPosition.x - planet_size);
        var y = Random.Range(GenSettings.cam_ZeroPosition.y + planet_size, -GenSettings.cam_ZeroPosition.y - planet_size);

        transform.position = new Vector2(x, y);
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Planet")
        {
            Resize();
            Reposition();
        }
    }

    void Update()
    {
        Marking();
        if (belongs == "Player")
        {
            if (GenSettings.from == gameObject)
            {
                GlowingOn();
            }
            if (GenSettings.from != gameObject)
            {
                GlowingOff();
            }
        }
    }

    public void GlowingOff()
    {
        GetComponent<SpriteRenderer>().material = marker_Player;
    }
    public void GlowingOn()
    {
        GetComponent<SpriteRenderer>().material = marker_Glow_Player;
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        if (GenSettings.from == null && belongs == "Player")
        {
            Debug.Log(pointerEventData.pointerPress.gameObject.GetComponent<PlanetSript>().belongs);
            GenSettings.from = pointerEventData.pointerPress.gameObject;
        }
        else if (GenSettings.from == gameObject)
        {
            GenSettings.from = null;
        }
        else if (GenSettings.from != null)
        {
            Debug.Log(pointerEventData.pointerPress.gameObject.GetComponent<PlanetSript>().belongs);
            GenSettings.to = pointerEventData.pointerPress.gameObject;
            GenSettings.from.GetComponent<PlanetSript>().SendShip(GenSettings.from.GetComponent<PlanetSript>().planet_ship / 2);
        }
        else 
        {
            GenSettings.from = null;
        }
    }

    void Marking()
    {
        //Switching materials on planets
        if (belongs == "Player")
        {
            gameObject.GetComponent<SpriteRenderer>().material = marker_Player;
        }
        else if (belongs == "Enemy")
        {
            gameObject.GetComponent<SpriteRenderer>().material = marker_Enemy;
        }
    }

    void SendShip(int count)
    {
        GenSettings.ShipAttack(count, belongs);
    }



    private void FixedUpdate()
    {
        if (belongs != "Neutral")
        {
            Timer += Time.deltaTime;
            if (Timer >= DelayAmount)
            {
                Timer = 0f;
                planet_ship += planet_ship_construct;
            }
        }

        info.text = planet_ship.ToString();
    }
}
