using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using QFSW.MOP2;

public class GenerateSettings : MonoBehaviour
{
    #region Generate settings
    public int maxPlanets;
    public int minPlanets;

    [Tooltip("Screen size divider to calculate the size of planets on screen")]
    [SerializeField] private float dividerRatioSize;

    [HideInInspector] public float minPlanetSize;
    [HideInInspector] public float maxPlanetSize;

    [SerializeField] private GameObject planet;
    private List<GameObject> planetPool = new List<GameObject>();
    #endregion

    #region Other
    [HideInInspector] public Vector2 cam_ZeroPosition;
    [HideInInspector] public Camera cam;

    [HideInInspector] public GameObject from = null;
    [HideInInspector] public GameObject to = null;

    //Pacckage MOP2 (easy ObjectPolling)
    private MasterObjectPooler poolMaster;
    [SerializeField] private ObjectPool ships;
    [HideInInspector] public string belong = "Neutral";
    #endregion

    void Awake()
    {
        cam = Camera.main;
        cam.GetComponent<Camera>().orthographicSize = cam.pixelWidth;
        cam_ZeroPosition = cam.ScreenToWorldPoint(Vector2.zero); //Get zero coordinate from screen to World

        //Limiting the size of the planets
        if (cam.pixelWidth > cam.pixelHeight)
        {
            minPlanetSize = cam.pixelWidth / dividerRatioSize;
            maxPlanetSize = cam.pixelWidth / dividerRatioSize - 3;
        }
        else
        {
            minPlanetSize = cam.pixelHeight / dividerRatioSize;
            maxPlanetSize = cam.pixelHeight / dividerRatioSize - 3;
        }
        GenerateGalactic(Random.Range(minPlanets, maxPlanets));

        poolMaster = GetComponent<MasterObjectPooler>();
        ships.Initialize();
    }


    void GenerateGalactic(int planetCount)
    {
        //Neutral planet
        for (int i = 0; i < planetCount - 2; i++)
        {
            planetPool.Add(planet);
            planetPool[i].GetComponent<PlanetSript>().belongs = "Neutral";
            planetPool[i].SetActive(true);
            Instantiate(planetPool[i], new Vector3(0, 0, 0), Quaternion.identity).transform.SetParent(transform);
        }

        //Player planet
        planetPool.Add(planet);
        planetPool[planetCount - 2].GetComponent<PlanetSript>().belongs = "Player";
        planetPool[planetCount - 2].SetActive(true);
        Instantiate(planetPool[planetCount - 2], new Vector3(0, 0, 0), Quaternion.identity).transform.SetParent(transform);

        //Enemy planet
        planetPool.Add(planet);
        planetPool[planetCount - 1].GetComponent<PlanetSript>().belongs = "Enemy";
        planetPool[planetCount - 1].SetActive(true);
        Instantiate(planetPool[planetCount - 1], new Vector3(0, 0, 0), Quaternion.identity).transform.SetParent(transform);
    }

    public void ShipAttack(int count, string belong)
    {
        this.belong = belong;

        from.GetComponent<PlanetSript>().planet_ship -= count;
        for (int i = 0; i < count; i++)
        {
            ships.GetObject(from.transform.position);
        }
        from = null;
        to = null;
    }
}
