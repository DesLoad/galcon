using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    #region Serialization
    [HideInInspector] public GameObject galactic;
    [HideInInspector] public GameObject from;
    [HideInInspector] public GameObject to;
    #endregion

    #region Ship settings
    public Sprite sprite_Enemy;
    public Sprite sprite_Player;
    [HideInInspector] public string belong;
    public float ship_Speed;
    #endregion

    private void Awake()
    {
        galactic = GameObject.FindGameObjectWithTag("Gen");
        //Initialize information from planet
        from = galactic.GetComponent<GenerateSettings>().from;
        to = galactic.GetComponent<GenerateSettings>().to;
        belong = galactic.GetComponent<GenerateSettings>().belong;

        //Initialize position and rotation
        if (from != null)
        {
            transform.position = from.transform.position;
            Rotate();
        }
        //Spriting
        if (belong == "Player")
            GetComponent<SpriteRenderer>().sprite = sprite_Player;
        else
            GetComponent<SpriteRenderer>().sprite = sprite_Enemy;
    }

    private void FixedUpdate()
    {
        if (from != null && to != null)
            transform.position = Vector3.MoveTowards(transform.position, to.transform.position, ship_Speed);
    }

    private void Update()
    {
        Rotate();
    }

    void Rotate()
    {
        var offset = -90f;
        Vector2 direction = (Vector2)to.transform.position - (Vector2)transform.position;
        direction.Normalize();
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg; 
        Quaternion rotation = Quaternion.AngleAxis(angle + offset, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 2f * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject == to)
        {
            if (other.gameObject.GetComponent<PlanetSript>().planet_ship > 0 
                && other.gameObject.GetComponent<PlanetSript>().belongs != belong)
            {
                gameObject.SetActive(false);
                transform.position = Vector2.zero;

                other.gameObject.GetComponent<PlanetSript>().planet_ship--;
            }
            else
            {
                other.gameObject.GetComponent<PlanetSript>().belongs = belong; 
                other.gameObject.GetComponent<PlanetSript>().planet_ship++;
            }
            from = null;
            to = null;
            belong = null;
            gameObject.SetActive(false);
        }
    }
    private void OnEnable()
    {
        Awake();
    }
}
